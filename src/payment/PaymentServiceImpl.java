package payment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class PaymentServiceImpl {

	private final Logger logger = Logger.getLogger("dbConnection");
	private String driver = "org.apache.derby.jdbc.ClientDriver";
	private String protocol = "jdbc:derby://localhost:1527/";
	private Connection con;
	private Statement st;

	public PaymentServiceImpl() {
	}

	// Method to check if customer's balance is valid for processing payment
	// If true, deduct amount from balance
	public boolean checkBalance(String username, double balance, double total) {
		boolean validBalance = false;

		if (balance >= total) {
			
			double newBalance = balance - total;
			
			try {
				// Instantiate driver
				Class.forName(driver).newInstance();

				con = DriverManager.getConnection(protocol + "MyDB;create=true");
				st = con.createStatement();

				// Query and update customer balance
				st.execute("UPDATE Customer SET balance=" + newBalance
						+ " WHERE username='" + username + "'");
				
				// Release resources
				st.close();
				con.close();
			} catch (Exception e) {
				logger.info("Error" + e.toString());
			}
			
			validBalance = true;
		}
		return validBalance;
	}


}
